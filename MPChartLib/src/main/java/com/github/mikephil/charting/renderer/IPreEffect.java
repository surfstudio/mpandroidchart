package com.github.mikephil.charting.renderer;

import android.graphics.Paint;

import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

/**
 *  Customize paint before rendering
 */

public interface IPreEffect {
    void render(LineDataProvider mChart,
                ChartAnimator mAnimator,
                Paint mRenderPaint,
                ILineDataSet dataSet);
}

package com.github.mikephil.charting.renderer;

import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;

import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Transformer;

/**
 * Add gradient for line based on yAxis value
 */

public class GradientLinePreEffect implements IPreEffect {
    private int[] mGradientColor = new int[0];
    private float baseLine;
    private Mode mode;
    private double spread;

    public GradientLinePreEffect(int[] mGradientColor, float baseLine, Mode mode, double spread) {
        this.mGradientColor = mGradientColor;
        this.baseLine = baseLine;
        this.mode = mode;
        this.spread = spread;
    }

    @Override
    public void render(LineDataProvider chart, ChartAnimator animator, Paint renderPaint, ILineDataSet dataSet) {
        if (mGradientColor.length > 0) {
            Transformer trans = chart.getTransformer(dataSet.getAxisDependency());
            float phaseY = animator.getPhaseY();
            float min;
            float max;
            switch (mode) {
                case PERCENT:
                    min = (float) (baseLine * (1 - spread / 100) * phaseY);
                    max = (float) (baseLine * (1 + spread / 100) * phaseY);
                    break;
                case VALUE:
                    min = (float) (baseLine - spread * 1.5) * phaseY;
                    max = (float) (baseLine + spread * 1.5) * phaseY;
                    break;
                default:
                    throw new IllegalStateException("Unknown mode: " + mode);
            }
            float[] points = new float[]{0, min, 0, max};

            trans.pointValuesToPixel(points);
            renderPaint.setShader(new LinearGradient(0,
                    points[1],
                    0,
                    points[3],
                    mGradientColor,
                    null,
                    Shader.TileMode.CLAMP));
        }
    }

    /**
     * Type of spread value
     */
    public enum Mode {
        /**
         * spread value correspond part of baseLine value
         */
        PERCENT,
        /**
         * fix spread size
         */
        VALUE
    }
}

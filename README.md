# [**MPAndroidChart**](https://github.com/PhilJay/MPAndroidChart)

Библиотека для построения графиков

## Работа с Git
Для работы используются 2 remote
* origin https://github.com/PhilJay/MPAndroidChart (репо оригинала)
* surf https://bitbucket.org/surfstudio/mpandroidchart.git (репо студии)

**origin** используется для синхронизации с орининалом
**surf** для работы с локальными изменениями

## Работа с Gradle

### Деплой
```
./gradlew clean MPChartLib:uploadArchives
```

### Использование в проекте 

```
implement 'ru.surfstudio.external:MPChartLib:{$version}'
```


